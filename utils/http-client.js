"use strict";

const https = require('https');
const Fs = require('fs')  
const Axios = require('axios');

const axios = require('axios').create({
  httpsAgent: new https.Agent({
    rejectUnauthorized: false // does not check self-signed certs
  })
});

/**
 * Wrapper on the Axios Library. We can inject Authorization headers and other items here.
 */
class HttpClient {

  constructor(config) {
    this.config = config ? config : {};
  }

  get(url, config) {
    const end = () => { };
    return axios.get(url, config)
      .then((res) => {
        end();
        return res;
      }).catch(err => {
        // console.log('ERROR:HttpClient:get:' + err.message);
        throw err.message;
      });
  }

  delete(url, config) {
    const end = () => { };
    return axios.delete(url, config)
      .then((res) => {
        end();
        return res;
      });
  }

  post(url, data, config) {
    const end = () => { };
    return axios.post(url, data, config)
      .then((res) => {
        
        end();
        return res;
      }).catch(err => {
        // console.log('http-client:' + err + ', url=' + url);
        end();
        throw err;
      });
  }

  put(url, data, config) {
    const end = () => { };
    return axios.put(url, data, config)
      .then((res) => {
        end();
        return res;
      });
  }

  patch(url, data, config) {
    const end = () => { };
    return axios.patch(url, data, config)
      .then((res) => {
        end();
        return res;
      });
  }

  async downloadImage (url, path) {  
    //const url = 'https://unsplash.com/photos/AaEQmoufHLk/download?force=true'
    
    
    const writer = Fs.createWriteStream(path);

    const response = await Axios({
      url,
      method: 'GET',
      responseType: 'xml'
    })

    response.data.pipe(writer);

    return new Promise((resolve, reject) => {
      writer.on('finish', () => {
        resolve('done');
      });
      writer.on('error', (err) => {
        reject(err);
      });
    })
  }
}

module.exports = HttpClient;
