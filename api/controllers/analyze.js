'use strict'

var path = require('path');
const { resolve } = require('path');

var fs = require('fs');

const HttpClient = require('../../utils/http-client.js');

const DLP_SERVICE_URL = 'http://localhost:8080/analyze';

async function analyzeFile(req, res){
    try{

        console.log('analyzing: ' + req.swagger.params.file.raw.originalname);

        let customer = req.swagger.params.customer.raw || 'demo';
        let extractEmbedded = req.swagger.params.extractEmbedded.raw || true;
        
        console.log('customer: ' + customer);  
        console.log('extractEmbedded: ' + extractEmbedded);

        let fileFullName = path.join(__dirname, '../../uploadedFiles/', req.swagger.params.file.raw.originalname);
        let buffer = req.swagger.params.file.raw.buffer;

        fs.writeFileSync(fileFullName, buffer, "binary");

        let client = new HttpClient();

        let dlpConfigPath = resolve('./itm-config.json');

        let body = {
            path: fileFullName,
            extractEmbedded: extractEmbedded,
            // parserOnly: false,
            dlpConfig: {
                path: dlpConfigPath,
                customer_configuration: customer
            }            
        }

        let results = await client.post(DLP_SERVICE_URL, body);

        res.status(200).json({_status: {status: '200', code: 'error: none'}, data: results.data });

    }catch(err){
        console.log(err);
        res.json({_status: {status: '500', code: 'error: analyzeFile'}, error: err });
    }

}

module.exports = {
    analyzeFile: analyzeFile
}