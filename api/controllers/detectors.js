'use strict'

var path = require('path');
var fs = require('fs');

function getCustomersList(req, res){ 
    try{

        let dlpConfig = fs.readFileSync('./itm-config.json', {encoding:'utf8', flag:'r'});

        let customerConfiguration = JSON.parse(dlpConfig).customer_configurations;

        let customers = Object.keys(customerConfiguration);

        res.status(200).json({_status: {status: '200', code: 'error: none'}, data: customers });

    }catch(err){
        console.log(err);
        res.json({_status: {status: '500', code: 'error: getDetectorsByCustomer'}, error: err });
    }
 
}

module.exports = {
    getCustomersList: getCustomersList
}