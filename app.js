'use strict';

const fsExtra = require('fs-extra')


var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};

try{
  fsExtra.emptyDirSync('./uploadedFiles');
}catch(err){
  console.log(err);
} 

SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }
 
  // app.use(cors());
  // install middleware
  swaggerExpress.register(app);

  var port = process.env.PORT || 8081;
  app.listen(port);

  if (swaggerExpress.runner.swagger.paths['/hello']) {
    console.log('server listing at http://127.0.0.1:' + port);
  }
});
